﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMotorcycle.Object;
using MyMotorcycle.DataAccess;

namespace MyMotorcycle.RelationShip
{
    class FuzzySystemInputRelation
    {
        private int idFuzzySystem;

        public int IdFuzzySystem
        {
            get { return idFuzzySystem; }
            set { idFuzzySystem = value; }
        }

        private List<string> propertiesList;
        public List<string> PropertiesList
        {
            get { return propertiesList; }
            set { propertiesList = value; }
        }

        public FuzzySystemInputRelation(string detail)
        {
            string[] temp = detail.Split(':');
            IdFuzzySystem = int.Parse(temp[0]);
            PropertiesList = (temp[1].Split(',')).ToList();
        }

        public string getInputFuzzySystem(Motorcycle motor)
        {
            string result = "[ ";
            foreach (string i in PropertiesList)
            {
                result += getStringValueOfProperty(motor, i) + " ";
            }
            return result + "]";
        }

        private string getStringValueOfProperty(Motorcycle motor, String strProperty)
        {
            switch (strProperty)
            {
                case "FuelComsumption":
                    return motor.FuelConsumption.ToString();

                case "CylinderCapacity":
                    return motor.CylinderCapacity.ToString();

                case "NumberOfCylinder":
                    return motor.NumberOfCylinder.ToString();

                case "FuelSupplySystem":
                    return DataMotor.getValueFuel(motor.FuelSupplySystem).ToString();

                case "Type":
                    return DataMotor.getValueType(motor.Type).ToString();

                case "BrakeSystem":
                    return DataMotor.getValueBrake(motor.BrakeType).ToString();

                case "NeutonMeter":
                    return motor.MaximumTorque.NeutonMeter.ToString();

                case "RpmTorque":
                    return motor.MaximumTorque.Rpm.ToString();

                case "HorsePower":
                    return motor.MaximumPower.HorsePower.ToString();

                case "RpmPower":
                    return motor.MaximumPower.Rpm.ToString();

                case "ClutchSystem":
                    return DataMotor.getValueClutch(motor.ClutchSystem).ToString();

                case "Price":
                    return (motor.Price / 1000000f).ToString();

                case "SeatHeight":
                    return motor.SeatHeigh.ToString();

                case "FuelTankCapacity":
                    return motor.FuelTankCapacity.ToString();

                case "CoolingSystem":
                    return DataMotor.getValueCooling(motor.CoolingSystem).ToString();

                case "WetWeight":
                    return motor.WetWeight.ToString();
            }
            return "null";
        }

    }
}
