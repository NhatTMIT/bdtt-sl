﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMotorcycle.Object;
using System.Windows.Forms;

namespace MyMotorcycle.Object
{
    class MotorResult
    {
        float[] vectorBase;
        float[] vectorReal;
        string[] image;
        public string[] Image
        {
            get { return image; }
            set { image = value; }
        }

        Motorcycle motor;
        internal Motorcycle Motor
        {
            get { return motor; }
            set { motor = value; }
        }

        double scorce;
        public double Scorce
        {
            get { return scorce; }
            set { scorce = value; }
        }

        public MotorResult(float[] vectorBase, float[] vectorReal, Motorcycle motor)
        {
            DataAccess.DataMotor data = new DataAccess.DataMotor();
            this.motor = data.getMotorcycleWithFullInfo(motor.Id);
            this.vectorBase = vectorBase;
            this.vectorReal = vectorReal;

            for (int i = 0; i < vectorBase.Length; i++)
            {
                if (vectorBase[i] == 0)
                    vectorReal[i] = 0;
            }

            scorce = computeScore();
            image = motor.Image.Split(',');
        }

        private double computeScore()
        {
            float scalar = 0;

            float lenghtA = 0;

            float lenghtB = 0;

            string str = "";
            string str2 = "";
            int count = 0;
            for (int i = 0; i < vectorBase.Length; i++)
            {
                //str += vectorBase[i] + " ";
                //str2 += vectorReal[i] + " ";
                //if (vectorBase[i] != 0)
                //{
                //    scalar += vectorBase[i] * vectorReal[i];
                //    lenghtA += vectorBase[i] * vectorBase[i];
                //    lenghtB += vectorReal[i] * vectorReal[i];
                //}

                if (vectorBase[i] != 0)
                {
                    scalar += vectorReal[i];
                    count++;
                }

            }
            //MessageBox.Show(str + "\n" + str2 + "\n" + scalar + "\n" + lenghtA + "\n" + lenghtB + "\n" + scalar / Math.Acos(Math.Sqrt(2) / 2f));
            //return Math.Acos(scalar/(Math.Sqrt(lenghtA)*Math.Sqrt(lenghtB)));
            return scalar / (float)count;
        }
    }
}
