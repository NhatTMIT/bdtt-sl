﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMotorcycle.Object
{
    class Torque
    {
        private float neutonMeter;
        public float NeutonMeter
        {
            get { return neutonMeter; }
            set { neutonMeter = value; }
        }

        private int rpm;
        public int Rpm
        {
            get { return rpm; }
            set { rpm = value; }
        }

        public Torque(String value)
        {
            string[] temp = value.Split('@');
            neutonMeter = float.Parse(temp[0]);
            rpm = int.Parse(temp[1]);
        }

        public override string ToString()
        {
            return neutonMeter + " Nm @ " + rpm + " rpm";
        }
    }
}
