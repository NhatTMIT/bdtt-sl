﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMotorcycle.Object
{
    class Power
    {
        private float horsePower;
        public float HorsePower
        {
            get { return horsePower; }
            set { horsePower = value; }
        }

        private int rpm;
        public int Rpm
        {
            get { return rpm; }
            set { rpm = value; }
        }

        public Power(String value)
        {
            string[] temp = value.Split('@');
            horsePower = float.Parse(temp[0]);
            rpm = int.Parse(temp[1]);
        }

        public override string ToString()
        {
            return HorsePower + " Hp @ " + rpm + " rpm";
        }
    }
}
