﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMotorcycle.Object
{
    class Motorcycle
    {
        private string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string company;
        public string Company
        {
            get { return company; }
            set { company = value; }
        }

        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        private int cylinderCapacity;
        public int CylinderCapacity
        {
            get { return cylinderCapacity; }
            set { cylinderCapacity = value; }
        }

        private int numberOfCylinder;
        public int NumberOfCylinder
        {
            get { return numberOfCylinder; }
            set { numberOfCylinder = value; }
        }

        private Torque maximumTorque;
        internal Torque MaximumTorque
        {
            get { return maximumTorque; }
            set { maximumTorque = value; }
        }

        private Power maximumPower;
        internal Power MaximumPower
        {
            get { return maximumPower; }
            set { maximumPower = value; }
        }

        private string clutchSystem;
        public string ClutchSystem
        {
            get { return clutchSystem; }
            set { clutchSystem = value; }
        }

        private string fuelSupplySystem;
        public string FuelSupplySystem
        {
            get { return fuelSupplySystem; }
            set { fuelSupplySystem = value; }
        }

        private string coolingSystem;
        public string CoolingSystem
        {
            get { return coolingSystem; }
            set { coolingSystem = value; }
        }

        private string brakeType;
        public string BrakeType
        {
            get { return brakeType; }
            set { brakeType = value; }
        }

        private int seatHeigh;
        public int SeatHeigh
        {
            get { return seatHeigh; }
            set { seatHeigh = value; }
        }

        private int groundClearance;
        public int GroundClearance
        {
            get { return groundClearance; }
            set { groundClearance = value; }
        }

        private float fuelTankCapacity;
        public float FuelTankCapacity
        {
            get { return fuelTankCapacity; }
            set { fuelTankCapacity = value; }
        }

        private float fuelConsumption;
        public float FuelConsumption
        {
            get { return fuelConsumption; }
            set { fuelConsumption = value; }
        }

        private int wetWeight;
        public int WetWeight
        {
            get { return wetWeight; }
            set { wetWeight = value; }
        }

        private string color;
        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        private int price;
        public int Price
        {
            get { return price; }
            set { price = value; }
        }


        private string image;
        public string Image
        {
            get { return image; }
            set { image = value; }
        }






    }
}
