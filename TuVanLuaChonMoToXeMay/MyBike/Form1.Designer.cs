﻿namespace MyMotorcycle
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabScene = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.cbbMaleFemale = new System.Windows.Forms.ComboBox();
            this.cbbCareer = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnBack = new System.Windows.Forms.Button();
            this.lvResult = new System.Windows.Forms.ListView();
            this.index = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pbShow = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbFSS = new System.Windows.Forms.Label();
            this.lbPrice = new System.Windows.Forms.Label();
            this.lbWetWeight = new System.Windows.Forms.Label();
            this.lbFuelComsumption = new System.Windows.Forms.Label();
            this.lbFTC = new System.Windows.Forms.Label();
            this.lbGroundClearance = new System.Windows.Forms.Label();
            this.lbSeatHeight = new System.Windows.Forms.Label();
            this.lbBreakSystem = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lbCoolingSystem = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lbClutchSystem = new System.Windows.Forms.Label();
            this.lbPower = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbTorque = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbNumberOfCylinder = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbCylinderCapacity = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbCompany = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabScene.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbShow)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabScene
            // 
            this.tabScene.Controls.Add(this.tabPage1);
            this.tabScene.Controls.Add(this.tabPage2);
            this.tabScene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabScene.Location = new System.Drawing.Point(0, 0);
            this.tabScene.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabScene.Name = "tabScene";
            this.tabScene.SelectedIndex = 0;
            this.tabScene.Size = new System.Drawing.Size(1354, 663);
            this.tabScene.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImage = global::MyMotorcycle.Properties.Resources.WINNER_150__8_;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage1.Controls.Add(this.txtPrice);
            this.tabPage1.Controls.Add(this.btnStart);
            this.tabPage1.Controls.Add(this.cbbMaleFemale);
            this.tabPage1.Controls.Add(this.cbbCareer);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.checkedListBox);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1346, 630);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(233, 235);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(213, 27);
            this.txtPrice.TabIndex = 7;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(161, 553);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(151, 29);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Tư vấn";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cbbMaleFemale
            // 
            this.cbbMaleFemale.DisplayMember = "0";
            this.cbbMaleFemale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbMaleFemale.FormattingEnabled = true;
            this.cbbMaleFemale.Items.AddRange(new object[] {
            "None",
            "Nam",
            "Nữ"});
            this.cbbMaleFemale.Location = new System.Drawing.Point(233, 137);
            this.cbbMaleFemale.Name = "cbbMaleFemale";
            this.cbbMaleFemale.Size = new System.Drawing.Size(213, 28);
            this.cbbMaleFemale.TabIndex = 5;
            this.cbbMaleFemale.ValueMember = "1";
            // 
            // cbbCareer
            // 
            this.cbbCareer.DisplayMember = "1";
            this.cbbCareer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCareer.FormattingEnabled = true;
            this.cbbCareer.Items.AddRange(new object[] {
            "None",
            "Học sinh - Sinh viên",
            "Doanh nhân",
            "Nhân viên văn phòng",
            "Nghệ sĩ",
            "Kĩ sư",
            "Người giao hàng",
            "Tài xế GrabBike/Uber"});
            this.cbbCareer.Location = new System.Drawing.Point(233, 44);
            this.cbbCareer.Name = "cbbCareer";
            this.cbbCareer.Size = new System.Drawing.Size(213, 28);
            this.cbbCareer.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Cambria", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(74, 325);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Những thuộc tính quan tâm:";
            // 
            // checkedListBox
            // 
            this.checkedListBox.Font = new System.Drawing.Font("Cambria", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Items.AddRange(new object[] {
            "Tiết kiệm nhiên liệu",
            "Khả năng đi đường dài",
            "Sức mạnh",
            "Tốc độ",
            "An toàn",
            "Dễ sửa chữa",
            "Dễ điều khiển"});
            this.checkedListBox.Location = new System.Drawing.Point(121, 361);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(238, 186);
            this.checkedListBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Cambria", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(74, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 23);
            this.label7.TabIndex = 1;
            this.label7.Text = "Giá:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cambria", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(69, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Giới tính:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cambria", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(69, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nghề Nghiệp:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnBack);
            this.tabPage2.Controls.Add(this.lvResult);
            this.tabPage2.Controls.Add(this.pbShow);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1346, 630);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(8, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 34);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Trở lại";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lvResult
            // 
            this.lvResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.index,
            this.name});
            this.lvResult.FullRowSelect = true;
            this.lvResult.Location = new System.Drawing.Point(1119, 42);
            this.lvResult.MultiSelect = false;
            this.lvResult.Name = "lvResult";
            this.lvResult.Size = new System.Drawing.Size(219, 578);
            this.lvResult.TabIndex = 2;
            this.lvResult.UseCompatibleStateImageBehavior = false;
            this.lvResult.View = System.Windows.Forms.View.Details;
            this.lvResult.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvResult_ItemSelectionChanged);
            this.lvResult.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvResult_MouseClick);
            // 
            // index
            // 
            this.index.Text = "Stt";
            this.index.Width = 51;
            // 
            // name
            // 
            this.name.Text = "Tên xe";
            this.name.Width = 155;
            // 
            // pbShow
            // 
            this.pbShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbShow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbShow.Location = new System.Drawing.Point(440, 42);
            this.pbShow.Name = "pbShow";
            this.pbShow.Size = new System.Drawing.Size(673, 578);
            this.pbShow.TabIndex = 1;
            this.pbShow.TabStop = false;
            this.pbShow.Click += new System.EventHandler(this.pbShow_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lbFSS);
            this.panel1.Controls.Add(this.lbPrice);
            this.panel1.Controls.Add(this.lbWetWeight);
            this.panel1.Controls.Add(this.lbFuelComsumption);
            this.panel1.Controls.Add(this.lbFTC);
            this.panel1.Controls.Add(this.lbGroundClearance);
            this.panel1.Controls.Add(this.lbSeatHeight);
            this.panel1.Controls.Add(this.lbBreakSystem);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.lbCoolingSystem);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.lbClutchSystem);
            this.panel1.Controls.Add(this.lbPower);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lbTorque);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.lbNumberOfCylinder);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.lbCylinderCapacity);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.lbType);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lbCompany);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lbName);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Font = new System.Drawing.Font("Cambria", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(8, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(426, 580);
            this.panel1.TabIndex = 0;
            // 
            // lbFSS
            // 
            this.lbFSS.AutoSize = true;
            this.lbFSS.Location = new System.Drawing.Point(205, 365);
            this.lbFSS.Name = "lbFSS";
            this.lbFSS.Size = new System.Drawing.Size(178, 20);
            this.lbFSS.TabIndex = 1;
            this.lbFSS.Text = "Hệ thống cấp nhiên liệu";
            // 
            // lbPrice
            // 
            this.lbPrice.AutoSize = true;
            this.lbPrice.Location = new System.Drawing.Point(205, 754);
            this.lbPrice.Name = "lbPrice";
            this.lbPrice.Size = new System.Drawing.Size(36, 20);
            this.lbPrice.TabIndex = 1;
            this.lbPrice.Text = "Giá:";
            // 
            // lbWetWeight
            // 
            this.lbWetWeight.AutoSize = true;
            this.lbWetWeight.Location = new System.Drawing.Point(205, 713);
            this.lbWetWeight.Name = "lbWetWeight";
            this.lbWetWeight.Size = new System.Drawing.Size(133, 20);
            this.lbWetWeight.TabIndex = 1;
            this.lbWetWeight.Text = "Trọng lượng ướt:";
            // 
            // lbFuelComsumption
            // 
            this.lbFuelComsumption.AutoSize = true;
            this.lbFuelComsumption.Location = new System.Drawing.Point(205, 670);
            this.lbFuelComsumption.Name = "lbFuelComsumption";
            this.lbFuelComsumption.Size = new System.Drawing.Size(183, 20);
            this.lbFuelComsumption.TabIndex = 1;
            this.lbFuelComsumption.Text = "Mức tiêu hao nhiên liệu:";
            // 
            // lbFTC
            // 
            this.lbFTC.AutoSize = true;
            this.lbFTC.Location = new System.Drawing.Point(205, 622);
            this.lbFTC.Name = "lbFTC";
            this.lbFTC.Size = new System.Drawing.Size(155, 20);
            this.lbFTC.TabIndex = 1;
            this.lbFTC.Text = "Dung tích bình xăng:";
            // 
            // lbGroundClearance
            // 
            this.lbGroundClearance.AutoSize = true;
            this.lbGroundClearance.Location = new System.Drawing.Point(205, 571);
            this.lbGroundClearance.Name = "lbGroundClearance";
            this.lbGroundClearance.Size = new System.Drawing.Size(157, 20);
            this.lbGroundClearance.TabIndex = 1;
            this.lbGroundClearance.Text = "Khoảng sáng gầm xe:";
            // 
            // lbSeatHeight
            // 
            this.lbSeatHeight.AutoSize = true;
            this.lbSeatHeight.Location = new System.Drawing.Point(205, 521);
            this.lbSeatHeight.Name = "lbSeatHeight";
            this.lbSeatHeight.Size = new System.Drawing.Size(113, 20);
            this.lbSeatHeight.TabIndex = 1;
            this.lbSeatHeight.Text = "Chiều cao yên:";
            // 
            // lbBreakSystem
            // 
            this.lbBreakSystem.AutoSize = true;
            this.lbBreakSystem.Location = new System.Drawing.Point(205, 474);
            this.lbBreakSystem.Name = "lbBreakSystem";
            this.lbBreakSystem.Size = new System.Drawing.Size(126, 20);
            this.lbBreakSystem.TabIndex = 1;
            this.lbBreakSystem.Text = "Hệ thống phanh:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(3, 713);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(147, 20);
            this.label34.TabIndex = 0;
            this.label34.Text = "Trọng lượng ướt:";
            // 
            // lbCoolingSystem
            // 
            this.lbCoolingSystem.AutoSize = true;
            this.lbCoolingSystem.Location = new System.Drawing.Point(205, 422);
            this.lbCoolingSystem.Name = "lbCoolingSystem";
            this.lbCoolingSystem.Size = new System.Drawing.Size(141, 20);
            this.lbCoolingSystem.TabIndex = 1;
            this.lbCoolingSystem.Text = "Hệ thống làm mát:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 670);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(196, 20);
            this.label32.TabIndex = 0;
            this.label32.Text = "Mức tiêu hao nhiên liệu:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(3, 753);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 20);
            this.label28.TabIndex = 0;
            this.label28.Text = "Giá:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 622);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(168, 20);
            this.label30.TabIndex = 0;
            this.label30.Text = "Dung tích bình xăng:";
            // 
            // lbClutchSystem
            // 
            this.lbClutchSystem.AutoSize = true;
            this.lbClutchSystem.Location = new System.Drawing.Point(205, 315);
            this.lbClutchSystem.Name = "lbClutchSystem";
            this.lbClutchSystem.Size = new System.Drawing.Size(131, 20);
            this.lbClutchSystem.TabIndex = 1;
            this.lbClutchSystem.Text = "Hệ thống Ly hợp:";
            // 
            // lbPower
            // 
            this.lbPower.AutoSize = true;
            this.lbPower.Location = new System.Drawing.Point(205, 264);
            this.lbPower.Name = "lbPower";
            this.lbPower.Size = new System.Drawing.Size(140, 20);
            this.lbPower.TabIndex = 1;
            this.lbPower.Text = "Công suất cực đại:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 571);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(173, 20);
            this.label26.TabIndex = 0;
            this.label26.Text = "Khoảng sáng gầm xe:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 521);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 20);
            this.label24.TabIndex = 0;
            this.label24.Text = "Chiều cao yên:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 365);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(196, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "Hệ thống cấp nhiên liệu:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 474);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(137, 20);
            this.label22.TabIndex = 0;
            this.label22.Text = "Hệ thống phanh:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 422);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(151, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "Hệ thống làm mát:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Hệ thống Ly hợp:";
            // 
            // lbTorque
            // 
            this.lbTorque.AutoSize = true;
            this.lbTorque.Location = new System.Drawing.Point(205, 217);
            this.lbTorque.Name = "lbTorque";
            this.lbTorque.Size = new System.Drawing.Size(158, 20);
            this.lbTorque.TabIndex = 1;
            this.lbTorque.Text = "Momen xoắn cực đại";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 264);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(150, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "Công suất cực đại:";
            // 
            // lbNumberOfCylinder
            // 
            this.lbNumberOfCylinder.AutoSize = true;
            this.lbNumberOfCylinder.Location = new System.Drawing.Point(205, 175);
            this.lbNumberOfCylinder.Name = "lbNumberOfCylinder";
            this.lbNumberOfCylinder.Size = new System.Drawing.Size(88, 20);
            this.lbNumberOfCylinder.TabIndex = 1;
            this.lbNumberOfCylinder.Text = "Số xy-lanh:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 217);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Momen xoắn cực đại:";
            // 
            // lbCylinderCapacity
            // 
            this.lbCylinderCapacity.AutoSize = true;
            this.lbCylinderCapacity.Location = new System.Drawing.Point(205, 129);
            this.lbCylinderCapacity.Name = "lbCylinderCapacity";
            this.lbCylinderCapacity.Size = new System.Drawing.Size(136, 20);
            this.lbCylinderCapacity.TabIndex = 1;
            this.lbCylinderCapacity.Text = "Dung tích xy-lanh";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số xy-lanh:";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(205, 90);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(46, 20);
            this.lbType.TabIndex = 1;
            this.lbType.Text = "Hãng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Dung tích xy-lanh:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "Dòng xe:";
            // 
            // lbCompany
            // 
            this.lbCompany.AutoSize = true;
            this.lbCompany.Location = new System.Drawing.Point(205, 49);
            this.lbCompany.Name = "lbCompany";
            this.lbCompany.Size = new System.Drawing.Size(46, 20);
            this.lbCompany.TabIndex = 1;
            this.lbCompany.Text = "Hãng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Hãng:";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(205, 11);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(53, 20);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Cambria", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tên xe:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 663);
            this.Controls.Add(this.tabScene);
            this.Font = new System.Drawing.Font("Cambria", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "My Motorcycle";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabScene.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbShow)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabScene;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbMaleFemale;
        private System.Windows.Forms.ComboBox cbbCareer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbTorque;
        private System.Windows.Forms.Label lbNumberOfCylinder;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbCylinderCapacity;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbCompany;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbFSS;
        private System.Windows.Forms.Label lbCoolingSystem;
        private System.Windows.Forms.Label lbPower;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbPrice;
        private System.Windows.Forms.Label lbWetWeight;
        private System.Windows.Forms.Label lbFuelComsumption;
        private System.Windows.Forms.Label lbFTC;
        private System.Windows.Forms.Label lbGroundClearance;
        private System.Windows.Forms.Label lbSeatHeight;
        private System.Windows.Forms.Label lbBreakSystem;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pbShow;
        private System.Windows.Forms.ListView lvResult;
        private System.Windows.Forms.ColumnHeader index;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lbClutchSystem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label7;



    }
}

