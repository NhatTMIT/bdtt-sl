﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
using MyMotorcycle.Object;
using MyMotorcycle.FuzzySystems;
using MyMotorcycle.RelationShip;


namespace MyMotorcycle.DataAccess
{
    class DataMotor
    {
        SQLiteConnection con;
        SQLiteCommand cmd;

        public static DataTable Brake = new DataTable();
        public static DataTable Clutch = new DataTable();
        public static DataTable Cooling = new DataTable();
        public static DataTable Factory = new DataTable();
        public static DataTable Fuel = new DataTable();
        public static DataTable Type = new DataTable();

        public DataMotor()
        {
            con = new SQLiteConnection("Data source = res/data/motoData.db");
            cmd = new SQLiteCommand();   
            con.Open();
            cmd.Connection = con;

            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            cmd.CommandText = "select * from Brake";
            adapter.SelectCommand = cmd;
            adapter.Fill(Brake);

            cmd.CommandText = "select * from Clutch";
            adapter.SelectCommand = cmd;
            adapter.Fill(Clutch);

            cmd.CommandText = "select * from Cooling";
            adapter.SelectCommand = cmd;
            adapter.Fill(Cooling);

            cmd.CommandText = "select * from Factory";
            adapter.SelectCommand = cmd;
            adapter.Fill(Factory);

            cmd.CommandText = "select * from Fuel";
            adapter.SelectCommand = cmd;
            adapter.Fill(Fuel);

            cmd.CommandText = "select * from Type";
            adapter.SelectCommand = cmd;
            adapter.Fill(Type);


        }

        public List<Motorcycle> getAllMotorcycle()
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            cmd.CommandText = "select * from Moto";
            adapter.SelectCommand = cmd;
            DataTable tb = new DataTable();
            adapter.Fill(tb);

            List<Motorcycle> motorList = new List<Motorcycle>();

            for (int i = 0; i < tb.Rows.Count; i++ )
            {
                motorList.Add(getMotorcycleFromDataRow(tb.Rows[i]));
            }
            return motorList;
        }

        private Motorcycle getMotorcycleFromDataRow(DataRow row)
        {
            Motorcycle motor = new Motorcycle();
            motor.Id = row[0].ToString();
            motor.Name = row[1].ToString();
            motor.Company = row[2].ToString();
            motor.Type = row[3].ToString(); ;
            motor.CylinderCapacity = int.Parse(row[4].ToString());
            motor.NumberOfCylinder = int.Parse(row[5].ToString());
            motor.MaximumTorque = new Torque(row[6].ToString());
            motor.MaximumPower = new Power(row[7].ToString());
            motor.ClutchSystem = row[8].ToString();
            motor.FuelSupplySystem = row[9].ToString();
            motor.CoolingSystem = row[10].ToString();
            motor.BrakeType = row[11].ToString();
            motor.SeatHeigh = int.Parse(row[12].ToString());
            motor.GroundClearance = int.Parse(row[13].ToString());
            motor.FuelTankCapacity = float.Parse(row[14].ToString());
            motor.FuelConsumption = float.Parse(row[15].ToString());
            motor.WetWeight = int.Parse(row[16].ToString());
            motor.Color = row[17].ToString();
            motor.Price = int.Parse(row[18].ToString());
            motor.Image = row[19].ToString();
            return motor;
        }

        public List<FuzzySystem> getAllFuzzySystem()
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            cmd.CommandText = "select * from FuzzySystem";
            adapter.SelectCommand = cmd;
            DataTable tb = new DataTable();
            adapter.Fill(tb);

            List<FuzzySystem> fuzzySystemList = new List<FuzzySystem>();

            for (int i = 0; i < tb.Rows.Count; i++)
            {
                fuzzySystemList.Add(getFuzzySystemFromDataRow(tb.Rows[i]));
            }
            return fuzzySystemList;
        }

        private FuzzySystem getFuzzySystemFromDataRow(DataRow row)
        {
            return new FuzzySystem(int.Parse(row[0].ToString()), row[1].ToString(), row[2].ToString());
        }

        public List<FuzzySystemInputRelation> getFuzzySystemInputRelation()
        {
            List<FuzzySystemInputRelation> inputRelation = new List<FuzzySystemInputRelation>();

            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            cmd.CommandText = "select * from Relation";
            adapter.SelectCommand = cmd;
            DataTable tb = new DataTable();
            adapter.Fill(tb);

            for (int i = 0; i < tb.Rows.Count; i++)
            {
                if (tb.Rows[i][0].ToString() == "FuzzySystemInput")
                    inputRelation.Add(new FuzzySystemInputRelation(tb.Rows[i][1].ToString()));
            }
            return inputRelation;
        }

        public Motorcycle getMotorcycleWithFullInfo(string id)
        {
            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            cmd.CommandText = @"select ID_Moto, name_Moto, name_Factory, name_Type, cylinder_Capacity, cylinder_Number, momen, power, name_Clutch, name_Fuel, name_Cooling, name_Brake, seat_height, ground_clearance, fuel_tank, overall_kmpl, weight_Moto, color_Moto, prices_Moto, image_Moto
from ((((((Moto a inner join Factory b on a.factory_Moto = b.ID_Factory) inner join Type c on a.type_Moto = c.ID_Type ) inner join Clutch d on a.clutch_Moto = d.ID_Clutch) inner join Fuel e on a.fuel_Moto = e.ID_Fuel) inner join Cooling f on a.Cooling_Moto = f.ID_Cooling) inner join Brake g on a.brake_Moto = g.ID_Brake)
where ID_Moto = '" + id + "'";

            adapter.SelectCommand = cmd;
            DataTable tb = new DataTable();
            adapter.Fill(tb);
            DataRow row = tb.Rows[0];

            Motorcycle motor = new Motorcycle();
            motor.Id = row[0].ToString();
            motor.Name = row[1].ToString();
            motor.Company = row[2].ToString();
            motor.Type = row[3].ToString(); ;
            motor.CylinderCapacity = int.Parse(row[4].ToString());
            motor.NumberOfCylinder = int.Parse(row[5].ToString());
            motor.MaximumTorque = new Torque(row[6].ToString());
            motor.MaximumPower = new Power(row[7].ToString());
            motor.ClutchSystem = row[8].ToString();
            motor.FuelSupplySystem = row[9].ToString();
            motor.CoolingSystem = row[10].ToString();
            motor.BrakeType = row[11].ToString();
            motor.SeatHeigh = int.Parse(row[12].ToString());
            motor.GroundClearance = int.Parse(row[13].ToString());
            motor.FuelTankCapacity = float.Parse(row[14].ToString());
            motor.FuelConsumption = float.Parse(row[15].ToString());
            motor.WetWeight = int.Parse(row[16].ToString());
            motor.Color = row[17].ToString();
            motor.Price = int.Parse(row[18].ToString());
            motor.Image = row[19].ToString();
            return motor;
        }

        public static int getValueBrake(string idBreak)
        {
            for (int i = 0; i < DataMotor.Brake.Rows.Count; i++)
            {
                if (idBreak == DataMotor.Brake.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }

        public static int getValueClutch(string idClutch)
        {
            for (int i = 0; i < DataMotor.Clutch.Rows.Count; i++)
            {
                if (idClutch == DataMotor.Clutch.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }

        public static int getValueCooling(string idCooling)
        {
            for (int i = 0; i < DataMotor.Cooling.Rows.Count; i++)
            {
                if (idCooling == DataMotor.Cooling.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }

        public static int getValueFactory(string idFactory)
        {
            for (int i = 0; i < DataMotor.Factory.Rows.Count; i++)
            {
                if (idFactory == DataMotor.Factory.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }

        public static int getValueFuel(string idFuel)
        {
            for (int i = 0; i < DataMotor.Fuel.Rows.Count; i++)
            {
                if (idFuel == DataMotor.Fuel.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }

        public static int getValueType(string idType)
        {
            for (int i = 0; i < DataMotor.Type.Rows.Count; i++)
            {
                if (idType == DataMotor.Type.Rows[i][0].ToString())
                    return i + 1;
            }
            return 0;
        }


    }
}
