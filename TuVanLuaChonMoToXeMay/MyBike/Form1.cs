﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyMotorcycle.FuzzySystems;
using System.Threading;
using MyMotorcycle.Business;
using MyMotorcycle.Object;

namespace MyMotorcycle
{
    public partial class Form1 : Form
    {
        MainBusiness mainBusiness;
        List<MotorResult> result;
        int indexImage = 0;
        bool start = false;
        public Form1()
        {
            InitializeComponent();
            mainBusiness = new MainBusiness();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tabScene.ItemSize = new Size(0, 1);
            tabScene.SizeMode = TabSizeMode.Fixed;
            cbbCareer.SelectedIndex = 0;
            cbbMaleFemale.SelectedIndex = 0;
            mainBusiness.initFuzzySystems();
        }

        private float[] getVectorBase()
        {
            string str = "";
            float[] vectorBase = new float[16];

            for (int i = 0; i < 7; i++)
            {
                if (cbbCareer.SelectedIndex - 1 == i)
                {
                    vectorBase[i] = 100;
                    start = true;
                }
                else
                    vectorBase[i] = 0;
                str += vectorBase[i] + " ";
            }

            for (int i = 7 ; i < 9; i++)
            {
                if (cbbMaleFemale.SelectedIndex + 7 - 1 == i)
                {
                    vectorBase[i] = 100;
                    start = true;
                }
                else
                    vectorBase[i] = 0;
                str += vectorBase[i] + " ";
            }

            for (int i = 0; i < checkedListBox.Items.Count; i++)
            {
                if (checkedListBox.GetItemChecked(checkedListBox.Items.IndexOf(checkedListBox.Items[i])) == true)
                {
                    vectorBase[i + 9] = 100;
                    start = true;
                }
                else
                    vectorBase[i + 9] = 0;
                str += vectorBase[i + 9] + " ";
            }

            return vectorBase;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            start = false;
            float[] vectorBase = getVectorBase();
            if (!start)
            {
                MessageBox.Show("Bạn chưa chọn tính năng nào\nHãy chọn các tính năng và thử lại");
                return;
            }
            result = mainBusiness.getResultAsses(vectorBase);
            //string str = "";
            int i = 0;

            int price = 0;

            if (txtPrice.Text != "")
            {
                price = int.Parse(txtPrice.Text);
                filter(price);
                if (result.Count < 1)
                {
                    MessageBox.Show("Không tìm thấy xe phù hợp");
                    return;
                }
            }



            sortScore();

            foreach (MotorResult motor in result)
            {
                i++;
                ListViewItem it = new ListViewItem(motor.Scorce.ToString());
                it.SubItems.Add(motor.Motor.Name);
                lvResult.Items.Add(it);
            }

            tabScene.SelectedIndex = 1;
            lvResult.Items[0].Selected = true;
            lvResult.Select();
            showInfoMotorCycle(result[0].Motor);
        }

        private void sortScore()
        {
            for (int i = 0; i < result.Count - 1; i++)
                for (int j = i + 1; j < result.Count; j++)
                {
                    if (result[i].Scorce < result[j].Scorce)
                    {
                        MotorResult temp = result[i];
                        result[i] = result[j];
                        result[j] = temp;
                    }
                }
        }

        private void filter(int price)
        {
            for (int i = 0; i < result.Count; i++)
            {
                if (result[i].Motor.Price > price)
                {
                    result.RemoveAt(i);
                    i--;
                }
            }
        }

        private void lvResult_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void lvResult_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (lvResult.SelectedItems.Count > 0)
            {
                int index = lvResult.Items.IndexOf(lvResult.SelectedItems[0]);
                showInfoMotorCycle(result[index].Motor);
            }
        }

        const string path = "res/image/";

        private void showInfoMotorCycle(Motorcycle motor)
        {
            lbName.Text = motor.Name;
            lbCompany.Text = motor.Company;
            lbType.Text = motor.Type;
            lbCylinderCapacity.Text = motor.CylinderCapacity.ToString() + " cc";
            lbNumberOfCylinder.Text = motor.NumberOfCylinder.ToString();
            lbTorque.Text = motor.MaximumTorque.ToString();
            lbPower.Text = motor.MaximumPower.ToString();
            lbClutchSystem.Text = motor.ClutchSystem;
            lbFSS.Text = motor.FuelSupplySystem;
            lbCoolingSystem.Text = motor.CoolingSystem;
            lbBreakSystem.Text = motor.BrakeType;
            lbSeatHeight.Text = motor.SeatHeigh + " mm";
            lbGroundClearance.Text = motor.GroundClearance + " mm";
            lbFTC.Text = motor.FuelTankCapacity + " lít";
            lbFuelComsumption.Text = motor.FuelConsumption + " lít/100km";
            lbWetWeight.Text = motor.WetWeight + " kg";
            lbPrice.Text = motor.Price + " VND";
            pbShow.Image = Image.FromFile(path + result[lvResult.Items.IndexOf(lvResult.SelectedItems[0])].Image[0]);
            pbShow.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            tabScene.SelectedIndex = 0;
            lvResult.Items.Clear();
        }

        private void pbShow_Click(object sender, EventArgs e)
        {
            if (indexImage < result[lvResult.Items.IndexOf(lvResult.SelectedItems[0])].Image.Length - 1)
                indexImage++;
            else
                indexImage = 0;
            pbShow.Image = Image.FromFile(path + result[lvResult.Items.IndexOf(lvResult.SelectedItems[0])].Image[indexImage].Trim());
            pbShow.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        
    }
}