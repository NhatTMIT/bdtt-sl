﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMotorcycle.DataAccess;
using MyMotorcycle.Object;
using MyMotorcycle.FuzzySystems;
using MyMotorcycle.RelationShip;
using System.Windows.Forms;

namespace MyMotorcycle.Business
{
    class MainBusiness
    {
        DataMotor data;
        public DataMotor Data
        {
            get { return data; }
            set { data = value; }
        }
        List<Motorcycle> motorList;
        List<FuzzySystem> fuzzySystems;
        List<FuzzySystemInputRelation> inputRelationList;

        float[] vectorBase;

        public List<FuzzySystem> FuzzySystems
        {
            get { return fuzzySystems; }
            set { fuzzySystems = value; }
        }

        public MainBusiness()
        {
            data = new DataAccess.DataMotor();
            motorList = data.getAllMotorcycle();
            fuzzySystems = data.getAllFuzzySystem();
            inputRelationList = data.getFuzzySystemInputRelation();

            foreach (FuzzySystem system in FuzzySystems)
            {
                system.setInputRelation(inputRelationList);
            }

            vectorBase = new float[16];
            for (int i = 0; i < vectorBase.Length; i++)
            {
                Random rand = new Random();
                vectorBase[i] = rand.Next(0,2) * 100;
            }
            
    }

        public void initFuzzySystems()
        {
            foreach (FuzzySystem system in fuzzySystems)
            {
                system.initFuzzySystem();
            }
        }

        public List<MotorResult> getResultAsses(float[] vectorBase)
        {
            this.vectorBase = vectorBase;
            List<MotorResult> motorResultList = new List<MotorResult>();
            //MessageBox.Show(motorList.Count + "");
            foreach (Motorcycle motor in motorList)
            {
                motorResultList.Add(new MotorResult(vectorBase, getVectorRealFromMotor(motor), motor)); 
            }

            return motorResultList;
        }

        private float[] getVectorRealFromMotor(Motorcycle motor)
        {
            List<float> vectorReal = new List<float>();
            foreach (FuzzySystem system in fuzzySystems)
            {
                vectorReal.AddRange(system.Assess(motor).ToList());
            }

            float[] result = vectorReal.ToArray();

            for (int i = 0; i < 16; i++)
            {
                if (vectorBase[i] == 0)
                    result[i] = 0;
            }
            return result;
        }



    }
}
