﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using MyMotorcycle.Object;
using System.IO;
using System.Windows.Forms;
using MyMotorcycle.RelationShip;

namespace MyMotorcycle.FuzzySystems
{
    class FuzzySystem
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string fisPath;

        private string nameSystem;

        public string NameSystem
        {
            get { return nameSystem; }
            set { nameSystem = value; }
        }

        private FuzzySystemInputRelation inputRelation;

        MLApp.MLApp fuelFuzzySystem;

        public FuzzySystem(int id, string name, string fisPath)
        {
            Id = id;
            NameSystem = name;
            this.fisPath = Path.GetFullPath(fisPath);
        }

        public void setInputRelation(List<FuzzySystemInputRelation> inputRelationList)
        {
            foreach (FuzzySystemInputRelation relation in inputRelationList)
            {
                if (relation.IdFuzzySystem == Id)
                {
                    inputRelation = relation;
                    break;
                }
            }
        }

        public void initFuzzySystem()
        {
            fuelFuzzySystem = new MLApp.MLApp();
            Thread thread = new Thread((ThreadStart) =>
            {
                fuelFuzzySystem.Execute(nameSystem  + " = readfis('" + fisPath + "')");
            });
            thread.Start();
        }

        
        public float[] Assess(Motorcycle motor)
        {
            string input = inputRelation.getInputFuzzySystem(motor);
            string strResult = fuelFuzzySystem.Execute(@"evalfis(" + input + ", " + nameSystem + ")");
            //MessageBox.Show(NameSystem + " / " + motor.Name + " / " + input + " / \n" + strResult);
            List<string> temp = strResult.Split('\n').ToList();
            for (int i = 0; i < temp.Count; i++)
            {
                if(temp[i].Trim() == "")
                {
                    temp.RemoveAt(i);
                    i--;
                }
            }
            
            temp = temp[temp.Count - 1].Trim().Replace("   "," ").Split(' ').ToList();
            //bool test = false;
            for (int i = 0; i < temp.Count; i++)
            {
                if (temp[i].Trim() == "")
                {
                    //test = true;
                    temp.RemoveAt(i);
                    i--;
                }
            }

            //if (test)
            //    MessageBox.Show(NameSystem + " / " + motor.Name + " / " + input + " / \n" + strResult);

            //try
            //{

                float[] result = new float[temp.Count];
                for (int i = 0; i < temp.Count; i++)
                {
                    result[i] = float.Parse(temp[i]);
                }
            //}
            //catch
            //{
            //    MessageBox.Show(NameSystem + " / " + motor.Name + " / " + input + " / \n" + strResult);
            //}
            return result;
        }

    }
}
